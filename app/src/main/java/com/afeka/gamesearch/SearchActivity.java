package com.afeka.gamesearch;

import android.os.Bundle;
import com.afeka.gamesearch.Controller.FILTER_BY;
import com.afeka.gamesearch.Controller.GameRestIntegration;
import com.afeka.gamesearch.Model.USERS;
import com.afeka.gamesearch.Model.VideoGame;
import com.afeka.gamesearch.View.GameAdapter;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;
import java.util.ArrayList;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class SearchActivity extends AppCompatActivity
        implements SearchFragment.OnFragmentInteractionListener,
                    GameEditFragment.OnFragmentInteractionListener,
                    AddGameFragment.OnFragmentInteractionListener,
                    WarningFragment.OnWarningInteractionListener,
                    GameRestIntegration.OnRestInteractionListener,GameAdapter.OnItemClickListener {

    private RecyclerView mRecyclerView;
    private GameAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;


    private FragmentManager fragmentManager;
    private SearchFragment searchFragment;
    private WarningFragment warningFragment;
    private AddGameFragment addGameFragment;

    private ArrayList<VideoGame> videoGameList;
    private String textFilter;
    private FILTER_BY filterBy;
    private FloatingActionButton fab;
    private GameRestIntegration gameRestIntegration;
    private UserManager userManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        textFilter = "";
        filterBy = FILTER_BY.ALL;
        videoGameList = new ArrayList<>();

        mRecyclerView = findViewById(R.id.gameRecyclerView);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mAdapter = new GameAdapter(videoGameList);
        mAdapter.setOnItemClickListener(this);

        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mAdapter);

        fragmentManager = getSupportFragmentManager();
        searchFragment = new SearchFragment();
        warningFragment = new WarningFragment();
        addGameFragment = new AddGameFragment();

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.app_name);


        userManager = new UserManager(getApplicationContext());

        fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragmentManager.beginTransaction().add(R.id.flContent, searchFragment).addToBackStack(null).commit();
                fab.hide();
            }
        });

        gameRestIntegration = new GameRestIntegration(this, this);
        gameRestIntegration.performSearch(textFilter, filterBy);
    }


    @Override
    public void onFragmentInteraction(String text, FILTER_BY filter) {
        fragmentManager.popBackStack();
        textFilter = text;
        filterBy = filter;
        gameRestIntegration.performSearch(textFilter, filterBy);
        fab.show();
    }


    @Override
    public void onRestGetComplete(ArrayList<VideoGame> videoGameList) {
        this.videoGameList.clear();
        this.videoGameList = videoGameList;
        mAdapter.setVideoGameList(this.videoGameList);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onRestAddComplete(VideoGame videoGame) {
        gameRestIntegration.performSearch(textFilter, filterBy);
    }

    @Override
    public void onRestDeleteComplete() {
        gameRestIntegration.performSearch(textFilter, filterBy);
    }

    @Override
    public void onRestUpdateComplete(VideoGame videoGame) {
        gameRestIntegration.performSearch(textFilter, filterBy);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        USERS type = userManager.getUserType();
        if (type == USERS.ADMIN) {
            getMenuInflater().inflate(R.menu.toolbar_admin, menu);
        } else if (type == USERS.PLAYER) {
            getMenuInflater().inflate(R.menu.toolbar_user, menu);
        }
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        Log.e("toolbar item:", id + "");
        if (id == R.id.action_disconnect2 || id == R.id.action_disconnect) {
            userManager.clearUser();
            Toast.makeText(getBaseContext(), "Logout!", Toast.LENGTH_SHORT).show();
            finish();
        } else if (id == R.id.action_add_game) {
            fragmentManager.beginTransaction().add(R.id.flContent, addGameFragment).addToBackStack(null).commit();
            fab.hide();
        } else if (id == R.id.action_delete_all) {
            fragmentManager.beginTransaction().add(R.id.flContent, warningFragment).addToBackStack(null).commit();
            fab.hide();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClick(int position) {
        USERS type = userManager.getUserType();
        fab.hide();
        if (type == USERS.ADMIN) {
            GameEditFragment gameEditFragment = new GameEditFragment(videoGameList.get(position));
            fragmentManager.beginTransaction().add(R.id.flContent, gameEditFragment).addToBackStack(null).commit();

        } else if (type == USERS.PLAYER) {
            GameInfoFragment gameInfo = new GameInfoFragment(videoGameList.get(position));
            fragmentManager.beginTransaction().add(R.id.flContent, gameInfo).addToBackStack(null).commit();
        }
    }



    @Override
    public void onFragmentClickOk() {
        gameRestIntegration.deleteGame(GameRestIntegration.DELETE_TYPE.ALL,null,userManager.getFullUser());
        Toast.makeText(getBaseContext(), "Deleted all games!", Toast.LENGTH_SHORT).show();
        fragmentManager.popBackStack();
        gameRestIntegration.performSearch(null,FILTER_BY.ALL);
        fab.show();
    }

    @Override
    public void onFragmentClickCancel() {
        fragmentManager.popBackStack();
        fab.show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        fragmentManager.popBackStack();
        fab.show();
    }

    @Override
    public void onFragmentClickUpdate(VideoGame videoGame) {
        gameRestIntegration.updateGame(videoGame,userManager.getFullUser());
        fragmentManager.popBackStack();
        fab.show();
    }

    @Override
    public void onFragmentClickDelete(VideoGame videoGame) {
        gameRestIntegration.deleteGame(GameRestIntegration.DELETE_TYPE.SINGLE,videoGame,userManager.getFullUser());
        fragmentManager.popBackStack();
        fab.show();
    }

    @Override
    public void onFragmentClickAdd(VideoGame videoGame) {
        gameRestIntegration.addGame(videoGame,userManager.getFullUser());
        fragmentManager.popBackStack();
        fab.show();
    }
}