package com.afeka.gamesearch;

import android.content.Context;
import android.content.res.TypedArray;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.afeka.gamesearch.Model.VideoGame;


public class GameInfoFragment extends Fragment {

    private VideoGame videoGame;

    private TextView nameView;
    private TextView yearView;
    private TextView genreView;
    private TextView companyView;

    private String[] genres;

    public GameInfoFragment() {
        // Required empty public constructor
    }

    public GameInfoFragment(VideoGame videoGame){
        this.videoGame = videoGame;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        genres = getResources().getStringArray(R.array.Genre);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_game_info, container, false);

        nameView = view.findViewById(R.id.game_info_fragment_title);
        genreView = view.findViewById(R.id.game_info_fragment_genre2);
        companyView = view.findViewById(R.id.game_info_fragment_company2);
        yearView = view.findViewById(R.id.game_info_fragment_year2);

        nameView.setText(videoGame.getGameName());
        genreView.setText(genres[videoGame.getGenre().ordinal()]);
        companyView.setText(videoGame.getCompany());
        yearView.setText(videoGame.getDeploymentYear()+"");


        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


}
