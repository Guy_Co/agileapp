package com.afeka.gamesearch;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.afeka.gamesearch.Model.GENRES;
import com.afeka.gamesearch.Model.VideoGame;


public class GameEditFragment extends Fragment {

    private VideoGame videoGame;
    private OnFragmentInteractionListener mListener;

    private EditText name;
    private Spinner genre;
    private EditText year;
    private EditText company;

    public GameEditFragment() {
        // Required empty public constructor
    }

    public GameEditFragment(VideoGame videoGame){
        this.videoGame = videoGame;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_game_edit, container, false);
        name = view.findViewById(R.id.game_edit_fragment_title);
        year = view.findViewById(R.id.game_edit_fragment_year2);
        company = view.findViewById(R.id.game_edit_fragment_company2);
        genre = view.findViewById(R.id.game_edit_fragment_genre2);

        name.setText(videoGame.getGameName());
        year.setText(videoGame.getDeploymentYear()+"");
        company.setText(videoGame.getCompany());
        genre.setSelection(videoGame.getGenre().ordinal());

        Button updateButton = view.findViewById(R.id.buttonUpdate);
        updateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                videoGame.setGameName(name.getText().toString());
                videoGame.setGenre(GENRES.values()[genre.getSelectedItemPosition()]);
                videoGame.setDeploymentYear(Integer.parseInt(year.getText().toString()));
                videoGame.setCompany(company.getText().toString());
                mListener.onFragmentClickUpdate(videoGame);
            }
        });
        Button deleteButton = view.findViewById(R.id.buttonDelete);
        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onFragmentClickDelete(videoGame);
            }
        });
        return view;
    }


    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnFragmentInteractionListener {
        void onFragmentClickUpdate (VideoGame videoGame);
        void onFragmentClickDelete (VideoGame videoGame);
    }
}
