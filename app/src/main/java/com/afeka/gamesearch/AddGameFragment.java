package com.afeka.gamesearch;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.afeka.gamesearch.Model.GENRES;
import com.afeka.gamesearch.Model.VideoGame;


public class AddGameFragment extends Fragment {
    private VideoGame videoGame;
    private OnFragmentInteractionListener mListener;

    private EditText name;
    private Spinner genre;
    private EditText year;
    private EditText company;

    public AddGameFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        videoGame = new VideoGame();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_game, container, false);
        name = view.findViewById(R.id.game_add_fragment_title);
        year = view.findViewById(R.id.game_add_fragment_year2);
        company = view.findViewById(R.id.game_add_fragment_company2);
        genre = view.findViewById(R.id.game_add_fragment_genre2);



        Button addButton = view.findViewById(R.id.buttonAdd);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                videoGame.setGameName(name.getText().toString());
                videoGame.setGenre(GENRES.values()[genre.getSelectedItemPosition()]);
                videoGame.setDeploymentYear(Integer.parseInt(year.getText().toString()));
                videoGame.setCompany(company.getText().toString());
                mListener.onFragmentClickAdd(videoGame);
            }
        });
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onResume() {
        super.onResume();
        name.setText("");
        year.setText("");
        company.setText("");
    }

    public interface OnFragmentInteractionListener {
        void onFragmentClickAdd(VideoGame videoGame);
    }
}
